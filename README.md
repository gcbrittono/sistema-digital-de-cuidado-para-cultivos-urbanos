# Sistema Digital para Cuidado de Plantas #

Las plantas, a diferencia de los humanos, no pueden comunicarse para avisar cuando necesitan agua, por ende, el simple descuido por varios días de esta necesidad da como resultado el deterioro y hasta la muerte de la misma.

Otras variables, como la temperatura del ambiente, nivel de oxígeno, nutrientes en el suelo y luz, son indispensables para el correcto desarrollo de la planta; que de la misma manera que el agua, la ausencia de estas pueden ser perjudiciales para las plantas.

# ¿Qué desventajas tiene el sistema tradicional de riego? #

- ¿Si el agua se acaba y se está ausente? 

- El diseño inadecuado del hueco (muy grande) puede hacer que el agua se acabe muy rápido, o que toda el agua salga, ahogando a la planta.

- Las botellas de plástico con líquido al contacto con la intemperie, liberan con el tiempo bisfenol A, un químico con toxicidad en estudio.


# Solución #

Utilizando las herramientas que nos brinda la electrónica, por medio de sensores y actuadores diseñaremos un sistema que permita a la planta abastecerse de agua, y por medio de una pantalla poder sensar variables como la humedad, temperatura y luminosidad.