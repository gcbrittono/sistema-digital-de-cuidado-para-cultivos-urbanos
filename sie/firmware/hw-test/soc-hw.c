#include "soc-hw.h"

uart_t   *uart0  = (uart_t *)     0x20000000;
timer_t  *timer0 = (timer_t *)    0x60000000;
gpio_t   *gpio0  = (gpio_t *)     0x40000000;
i2c_t    *i2c0   = (i2c_t *)      0x10000000;
serial_t *serial0 = (serial_t *)  0x30000000;




isr_ptr_t isr_table[32];


void tic_isr();
/***************************************************************************
 * IRQ handling
 */
void isr_null()
{
}

void irq_handler(uint32_t pending)
{
	int i;

	for(i=0; i<32; i++) {
		if (pending & 0x01) (*isr_table[i])();
		pending >>= 1;
	}
}

void isr_init()
{
	int i;
	for(i=0; i<32; i++)
		isr_table[i] = &isr_null;
}

void isr_register(int irq, isr_ptr_t isr)
{
	isr_table[irq] = isr;
}

void isr_unregister(int irq)
{
	isr_table[irq] = &isr_null;
}

/***************************************************************************
 * TIMER Functions
 */
void msleep(uint32_t msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN | TIMER_IRQEN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}


void nsleep(uint32_t nsec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000000000)*nsec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN| TIMER_IRQEN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}




void tic_init()
{
	// Setup timer0.0
	timer0->compare0 = (FCPU/1000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

/***************************************************************************
 * UART Functions
 */
void uart_init()
{
	//uart0->ier = 0x00;  // Interrupt Enable Register
	//uart0->lcr = 0x03;  // Line Control Register:    8N1
	//uart0->mcr = 0x00;  // Modem Control Register

	// Setup Divisor register (Fclk / Baud)
	//uart0->div = (FCPU/(57600*16));
}

char uart_getchar()
{   
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;
}

void uart_putchar(char c)
{
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = c;
}

void uart_putstr(char *str)
{
	char *c = str;
	while(*c) {
		uart_putchar(*c);
		c++;
	}
}

/*****************************************************************************
* GPIO Functions
*/

/*
int read_columna(){
	gpio0->ctrl = 0x10;
	return gpio0->in;
}

void write_fila(int code){
	gpio0->ctrl = 0x14;
	gpio0->out = code;
}

void write_buzzer(int signal){
	gpio0->ctrl = 0x18;
	gpio0->oe = signal;
}
*/
void initdir(int first, int direction){
	gpio0->oe = direction;
	gpio0->out = first; //inicializar gpio
	
}


int read_last(){
	int ultimo;
	ultimo = gpio0->last;
	return ultimo;
}

void write_fila(int code){
	
	char flag0;
	char flag1;
	char flag2;
	char flag3;
	char flag4;

	switch (code){
			
		case 1: 
			flag1 = 7 & (4 | read_last());	
			gpio0->out = flag1;
			break;
		case 2: 
			flag2 = 11 & (8 | read_last());	
			gpio0->out = flag2;
			break;
		case 4: 
			flag3 = 19 & (16 | read_last());	
			gpio0->out = flag3;
			break;
		case 8: 
			flag4 = 35 & (32 | read_last());	
			gpio0->out = flag4;
			break;
		default: 
			flag0 = 3 & read_last();
			gpio0->out = flag0;
			break;			
	
	}
}

char read_hums(){

	uint32_t inputh;
	uint32_t dataouth;
	uint32_t out;	
	
	inputh = 16320 & gpio0->in;
	dataouth = inputh>>6;
 	
	out = 100-(100*(dataouth-90))/165; 
		
	return out;
}


char read_columna(){
	
	uint32_t inputc;
	uint32_t dataoutc;
	inputc = 245760 & gpio0->in;
	dataoutc = inputc>>14;
	
	return dataoutc;		
}



void initled(){
	int doutled1 = 2 | read_last();
	gpio0->out = doutled1;	
}

void offled(){
	int doutled2 = 61 & read_last();
	gpio0->out = doutled2; 	
}

void initvalve(){
	int doutvalve1 = 1 | read_last();	
	gpio0->out = doutvalve1; 
}

void offvalve(){
	int doutvalve2 = 62 & read_last();
	gpio0->out = doutvalve2;
} 

void initclkadc(){
	gpio0->initadc = 1;
}


/******************************************************************************
* i2c Functons
*/


void i2c_write (unsigned char dir, unsigned char data)
{ 
   while(!((i2c0->ucr & I2C_DONE)));
   i2c0->write =  dir<<9|data;
}


char i2c_read ( char dir)
{

    while(!((i2c0->ucr & I2C_DONE)));
    i2c0->read=dir;
    nsleep(15); 
    while(!((i2c0->ucr & I2C_DONE)));
    return i2c0->i2c_data_out;

    
}


void i2c_clockfreq(char div)
{
      while(!((i2c0->ucr & I2C_DONE)));
      i2c0->divisor=div;

}

/******************************************************************************
* Serial Functons
*/

void serial_init()
{
     serial0->measure = 1;
     msleep(80);
}

char read_humidity()
{   
    serial_init(); 
    return serial0->humidity;
}

char read_temperature()
{
    serial_init();
    return serial0->temperature;
}

char read_sum()
{
    serial_init();
    return serial0->sum;
}

char good()
{    
     int ind = 0;
     int plus = serial0->temperature + serial0->humidity;
     if(plus == serial0->sum) ind = 1;
     else ind = 0;
     
     return ind;   
}

