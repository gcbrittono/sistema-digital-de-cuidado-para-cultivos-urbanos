/**
 * 
 */

#include "soc-hw.h"


//---------------------------------------------------------------------------
// LCD Functions
//---------------------------------------------------------------------------

void writeCharlcd (char letter) 
{
  char highnib;
  char lownib;
  highnib = letter&0xF0;
  lownib = letter&0x0F;

     i2c_write(0x3F,highnib|0b00001001);
     i2c_write(0x3F,highnib|0b00001101);
     i2c_write(0x3F,highnib|0b00001001);  

     i2c_write(0x3F,(lownib<<4)|0b00001001);
     i2c_write(0x3F,(lownib<<4)|0b00001101);
     i2c_write(0x3F,(lownib<<4)|0b00001001);
}




void writeCommandlcd (char command) 
{
  char highnib;
  char lownib;
  highnib = command&0xF0;
  lownib = command&0x0F;

     i2c_write(0x3F,highnib|0b00001000);
     i2c_write(0x3F,highnib|0b00001100);
     i2c_write(0x3F,highnib|0b00001000);  

     i2c_write(0x3F,(lownib<<4)|0b00001000);
     i2c_write(0x3F,(lownib<<4)|0b00001100);
     i2c_write(0x3F,(lownib<<4)|0b00001000);
}




void writeStringlcd (char *str) {
	char *c = str;
	while(*c) {
		writeCharlcd(*c);
		c++;
	}
}







// LCD Commands------------------------------------------

// LCD_I2C CONFIG
// DB7 DB6 DB5 DB4 CTRST EN RW RS

void clearDisplay() 
{
   writeCommandlcd(0b00000001);
}

void returnHome()
{
   writeCommandlcd(0b00000010);
   msleep(2);
}

// I/D = 1, S=0
void entryModeSet2()
{  
  
   writeCommandlcd(0b00000110);
   msleep(1);
}

// I/D = 1, S=1
void entryModeSet()
{  
   writeCommandlcd(0b00000111);
   msleep(1);
}


// I/D = 0, S=0
void entryModeSet3()
{  
   writeCommandlcd(0b00000100);
   msleep(1);
}

// I/D = 0, S=1
void entryModeSet4()
{  
   writeCommandlcd(0b00000101);
   msleep(1);
}


void displayOff()
{  
   writeCommandlcd(0b00001000);
   msleep(1);
}

// D=1, C=1, B=1
void displayOn()
{  

   writeCommandlcd(0b00001111);
   msleep(1);
}


// S/C = 0, R/L = 1
void cursorShiftRight()
{  
   writeCommandlcd(0b00010100);
   msleep(1);
}



// S/C = 0, R/L = 0
void cursorShiftLeft()
{  
   writeCommandlcd(0b00010000);
   msleep(1);
}


// S/C = 1, R/L = 1
void displayShiftRight()
{   

   writeCommandlcd(0b00011100);
   msleep(1);
}


// S/C = 1, R/L = 0
void displayShiftLeft()
{  
   writeCommandlcd(0b00011000);
   msleep(1);
}



// D/L = 0, N = 1, F = 0
//4-Bit mode, 2 lines, 5x8 dots
void functionSet()
{  

   writeCommandlcd(0b00101000);
   msleep(1);
}




   void displayAddress(uint8_t col, uint8_t row){
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > 2 ) {
		row = 2-1;    // we count rows starting w/0
	}
        writeCommandlcd (0x80|(col + row_offsets[row]));
}



void lcdInit ()
{  //1
   // LCD_I2C CONFIG
   // DB7 DB6 DB5 DB4 CTRST EN RW RS
   msleep(50);
   i2c_write(0x3F,0b00111000);
   i2c_write(0x3F,0b00111100);
   i2c_write(0x3F,0b00111000);
   msleep(5);
   
   //2
   i2c_write(0x3F,0b00111000);
   i2c_write(0x3F,0b00111100);
   i2c_write(0x3F,0b00111000);
   msleep(5);
   //3
   i2c_write(0x3F,0b00111000);
   i2c_write(0x3F,0b00111100);
   i2c_write(0x3F,0b00111000);
   msleep(1);
   //5
   i2c_write(0x3F,0b00101000);
   i2c_write(0x3F,0b00101100);
   i2c_write(0x3F,0b00101000);
   msleep(1);
   //6
   i2c_write(0x3F,0b00101000);
   i2c_write(0x3F,0b00101100);
   i2c_write(0x3F,0b00101000);
   msleep(1);
   //7
   i2c_write(0x3F,0b10001000);
   i2c_write(0x3F,0b10001100);
   i2c_write(0x3F,0b10001000);
   msleep(1);
   //8
   i2c_write(0x3F,0b00001000);
   i2c_write(0x3F,0b00001100);
   i2c_write(0x3F,0b00001000);
   msleep(1);
   //9
   i2c_write(0x3F,0b10001000);
   i2c_write(0x3F,0b10001100);
   i2c_write(0x3F,0b10001000);
   msleep(1);
   //10
   i2c_write(0x3F,0b00001000);
   i2c_write(0x3F,0b00001100);
   i2c_write(0x3F,0b00001000);
   msleep(2);
   //11
   i2c_write(0x3F,0b11111000);
   i2c_write(0x3F,0b11111100);
   i2c_write(0x3F,0b11111000);
   msleep(1);
   //12
   i2c_write(0x3F,0b00001000);
   i2c_write(0x3F,0b00001100);
   i2c_write(0x3F,0b00001000);
   msleep(1);
   //13
   i2c_write(0x3F,0b01101000);
   i2c_write(0x3F,0b01101100);
   i2c_write(0x3F,0b01101000);
   msleep(2);

}


//---------------------------------------------------------------------------
// RTC Functions
//---------------------------------------------------------------------------


//Función para obtener los segundos del RTC
char getSecondRTC ()
{  
        i2c_write(0x68,0);
        return i2c_read(0x68);
}

//Función para obtener los minutos del RTC
char getMinuteRTC ()
{
     i2c_write(0x68,1);
     return i2c_read(0x68);

}


//Función para obtener la hora del RTC
char getHourRTC ()
{
     i2c_write(0x68,2);
     return i2c_read(0x68);  

}

//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
// ASCII converter 
//---------------------------------------------------------------------------

char asciiConv (char number)
{
     return number+48;

}


//Funcion que devuelve el numero de potencias de 10 en el numero
char powerCount (char number, char tenpower)
{ 

    if (number == 0)
        return tenpower;
    else
        return powerCount (number/10,tenpower+1);
}


char returnHundreds (char number)
{
  char power = powerCount(number,0);
    if (power == 3){
       return (number/100);
    } 
    else return 0;
}


char returnTenths (char number)
{
    number = number%100;
    char power = powerCount(number,0);
    if (power >=2) {
       return (number/10);
    }
   else return 0;
}


char returnUnits (char number)
{
     number = number%100;
     return (number%10);
}




// LCD Functions print ------------------------------------------------------

void printnumberlcd (char number)
{
      char hundred;
      char tenth;
      char unit;

      char hundredascii;
      char tenthascii;
      char unitascii;
    
      hundred = returnHundreds(number);
      tenth = returnTenths(number);
      unit = returnUnits(number);
     
      hundredascii = asciiConv(hundred);
      tenthascii = asciiConv(tenth);
      unitascii = asciiConv(unit);
    
      writeCharlcd(hundredascii);
      writeCharlcd(tenthascii);
      writeCharlcd(unitascii);
} 

// Imprime segundo o minuto
void printnumberRTC (char number)
{
  char highnib;
  char lownib;
  highnib = number&0xF0;
  lownib = number&0x0F;
  
  writeCharlcd(asciiConv(highnib>>4));
  writeCharlcd(asciiConv(lownib));
} 

// Imprime hora
void printHourRTC (char number)
{
  char highnib;
  char lownib;
  highnib = number&0x10;
  lownib = number&0x0F;
  
  writeCharlcd(asciiConv(highnib>>4));
  writeCharlcd(asciiConv(lownib));
} 



//----------------------------------------------------------------------------
inline void writeint(uint32_t val)
{
	uint32_t i, digit;

	for (i=0; i<8; i++) {
		digit = (val & 0xf0000000) >> 28;
		if (digit >= 0xA) 
			uart_putchar('A'+digit-10);
		else
			uart_putchar('0'+digit);
		val <<= 4;
	}
}

void test2() {
    uart_putchar('b');   
}

void test() {
    uart_putchar('a');
    test2();
    uart_putchar('c');
} 

char glob[] = "Global";

volatile uint32_t *p;
volatile uint8_t *p2;

extern uint32_t tic_msec;


//---------------------------------------------------------------------------
// Keyboard functions
//---------------------------------------------------------------------------

uint32_t key_read_num(int val){

	int case1 = 1;
	int case2 = 1;
	uint32_t dato = 0;
	uint32_t clave = 0;

	int bandera1 = 0;
	int bandera2 = 0;
	int bandera3 = 0;
	int bandera4 = 0;

	int cont = 0;
	int temp1 = 0;
	int temp2 = 0;
	int temp3 = 0;
	int temp4 = 0;
	int temp5 = 0;
	int temp6 = 0;
	int temp7 = 0;
	int temp8 = 0;

	uint32_t i;
        uint32_t data1 = 0;
	uint32_t data2 = 0;

	for(i = 2 ; i>1; i++){
//==========================================================================================//
//=============================== key matricial ============================================//

		msleep(1);

		temp1 = bandera1;
		temp2 = bandera2;
		temp3 = bandera3;
		temp4 = bandera4;
		
		switch (case1){
    			case 1:	write_fila(1);
				msleep(1);
				case2 = read_columna();
				switch (case2){
	   				case 1:	dato = 1;
						bandera1 = 1;
						break;
		    			case 2: dato = 4;
						bandera1 = 1;
						break;
			    		case 4:	dato = 7;
						bandera1 = 1;
						break;
		    			case 8: dato = 10;
						bandera1 = 1;
						break;
					default:	dato = dato;
							bandera1 = 0;
							break;
				}
				break;
    			case 2: write_fila(2);
				msleep(1);
				case2 = read_columna();
				switch (case2){
	    				case 1:	dato = 2;
						bandera2 = 1;
						break;
			    		case 2: dato = 5;
						bandera2 = 1;
						break;
		    			case 4:	dato = 8;
						bandera2 = 1;
						break;
			    		case 8: dato = 0;
						bandera2 = 1;
						break;
					default:	dato = dato;
							bandera2 = 0;
							break;
				}
				break;
	    		case 3:	write_fila(4);
				msleep(1);
				case2 = read_columna();
				switch (case2){
	    				case 1:	dato = 3;
						bandera3 = 1;
						break;
		    			case 2: dato = 6;
						bandera3 = 1;
						break;
			    		case 4:	dato = 9;
						bandera3 = 1;
						break;
		    			case 8: dato = 11;
						bandera3 = 1;
						break;
					default:	dato = dato;
							bandera3 = 0;
							break;
				}
				break;
    			case 4:	write_fila(8);
				msleep(1);
				case2 = read_columna();
				switch (case2){
	    				case 1:	dato = 12;
						bandera4 = 1;
						break;
			    		case 2: dato = 13;
						bandera4 = 1;
						break;
		    			case 4:	dato = 14;
						bandera4 = 1;
						break;
			    		case 8: dato = 15;
						bandera4 = 1;
						break;
					default:	dato = dato;
							bandera4 = 0;
							break;
				}
				break;
			default:	write_fila(0);
					break;
		}

		temp5 = bandera1;
		temp6 = bandera2;
		temp7 = bandera3;
		temp8 = bandera4;

		if (val == 0){

			if ((temp1 == 0 && temp5 == 0)||(temp1 == 1 && temp5 == 1)||(temp2 == 0 && temp6 == 0)||(temp2 == 1 && temp6 == 1)||(temp3 == 0 && temp7 == 0)||(temp3 == 1 && temp7 == 1)||(temp4 == 0 && temp8 == 0)||(temp4 == 1 && temp8 == 1)){

			}
			if((temp1 == 0 && temp5 == 1)||(temp2 == 0 && temp6 == 1)||(temp3 == 0 && temp7 == 1)||(temp4 == 0 && temp8 == 1)){
				clave = dato;
				i = 0;
			}
			
			
		}

		
		if (val == 1) {

			if ((temp1 == 0 && temp5 == 0)||(temp1 == 1 && temp5 == 1)||(temp2 == 0 && temp6 == 0)||(temp2 == 1 && temp6 == 1)||(temp3 == 0 && temp7 == 0)||(temp3 == 1 && temp7 == 1)||(temp4 == 0 && temp8 == 0)||(temp4 == 1 && temp8 == 1)){

			}
			if((temp1 == 0 && temp5 == 1)||(temp2 == 0 && temp6 == 1)||(temp3 == 0 && temp7 == 1)||(temp4 == 0 && temp8 == 1)){

				if((dato == 14)||(dato == 15)||(dato == 10)||(dato == 11)){
				
				}
				else{
					if(dato == 12){
						clave = data2;
						clearDisplay();	
						msleep(10);
						i = 0;
						cont = 3;
						
					}
					if(dato == 13){
						
						data1 = 0;
						data2 = 0;						
						displayAddress(6,1);
						printnumberlcd(000);
						msleep(500);
						cont = -1;
										
					}
	
					cont = cont + 1;

					if (cont == 1){
						data1 = dato;
						displayAddress(6,1);
						printnumberlcd(data1);
						msleep(500);
					}
					if (cont == 2){
						data2 = dato + 10*data1;
						displayAddress(6,1);
						printnumberlcd(data2);
						//i = 0;
						msleep(500);
					}

				}
			}	

		}
		
		if (case1 == 4){
			case1 = 0;
		}

		case1 = case1 + 1;
	}
	return clave;
}



int main()
{

char rangominhum = 0;
char rangomaxhum = 0;

i2c_clockfreq(7);
lcdInit();
initdir(0, 63);
initclkadc();

displayAddress(5,0);
writeStringlcd("Cultive");
displayAddress(4,1);
writeStringlcd("Protector");
msleep(4000);
clearDisplay();
msleep(10);

displayAddress(0,0);
writeStringlcd("Minimal humidity");
displayAddress(6,1);
printnumberlcd(000);

rangominhum = key_read_num(1);
msleep(1000);

clearDisplay();
msleep(10);

displayAddress(0,0);
writeStringlcd("Maxim humidity");
displayAddress(6,1);
printnumberlcd(000);
msleep(500);

rangomaxhum = key_read_num(1);
msleep(10);

clearDisplay();
msleep(10);

displayAddress(0,0);
writeStringlcd("Tmp");
displayAddress(3,1);
writeStringlcd("C");
displayAddress(5,0);
writeStringlcd("Hum");
displayAddress(8,1);
writeStringlcd("%");
displayAddress(10,0);
writeStringlcd("Time");

while (1)
{

char num1 = read_hums(); 
char num2 = read_temperature();
char numt;

char minute = getMinuteRTC();
char hour = getHourRTC();

displayAddress(0,0);
writeStringlcd("Tmp");
displayAddress(3,1);
writeStringlcd("C");
displayAddress(5,0);
writeStringlcd("Hum");
displayAddress(8,1);
writeStringlcd("%");
displayAddress(10,0);
writeStringlcd("Time");

if(num2 > 15) { //funcion eliminar error temperatura
		numt = num2;		
	}


displayAddress(0,1);
printnumberlcd(numt);
displayAddress(5,1);
printnumberlcd(num1);
displayAddress(10,1);
printHourRTC(hour);
displayAddress(12,1);
writeStringlcd(":");
displayAddress(13,1);
printnumberRTC (minute);
displayAddress(15,1);
writeStringlcd("   ");
	
	if(num1 <= rangominhum){
	clearDisplay();
	msleep(10);
	displayAddress(0,0);
	writeStringlcd("watering plant");
	displayAddress(9,1);
	writeStringlcd("%");
	while(num1 <= rangomaxhum){
		initvalve();
		num1 = read_hums(); 

		msleep(10); 
		displayAddress(6,1);
		printnumberlcd(num1);
		//num1 = read_hums(); 	
		msleep(500);
		
	}
	clearDisplay();	
	}
	else offvalve();
		
	if(((hour >= 18) && (hour <= 23))||((hour >= 0)&&(hour <= 6))) initled(); //prende de noche
	else offled();
		
msleep(1000);	

	

}
}






